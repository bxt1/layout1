<?php
/**
 * @file
 * Functions for the ASIP install profile.
 *
 * Installing from clean/fresh database, run these before doing cim:
 *  // drush cset system.site uuid eafb0157-6ed7-4e68-95c3-c8cab15af77c
 *	// drush cset language.entity.en uuid 6a28da13-1707-4c0e-98b5-54a3cca54300
 *
 */

use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Entity\EntityMalformedException;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;

/**
 * Implements hook_theme_suggestions_alter().
 */
function asip_theme_suggestions_form_alter(array &$suggestions, array $variables) {
  $suggestions[] = "{$variables['theme_hook_original']}__{$variables['element']['#form_id']}";
}

